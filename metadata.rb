maintainer       "ADCI"
maintainer_email "info@adcisolutions.com"
license          "All rights reserved"
description      "Installs/Configures phpapp"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          "0.0.2"

depends "nginx"
depends "php"
depends "php-fpm"
depends "openssl"
depends "ohai"
depends "build-essential"
depends "mariadb"
depends "pdepend"
